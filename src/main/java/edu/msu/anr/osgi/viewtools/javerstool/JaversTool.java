package edu.msu.anr.osgi.viewtools.javerstool;

import org.apache.velocity.tools.view.tools.ViewTool;

import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;

/**
 * Velocity viewtool to provide methods from the Javers library.
 */
public class JaversTool implements ViewTool {

    /**
     * Initializes the viewtool.
     * @param initData Data necessary to initialize the tool.
     */
	@Override
	public void init(Object initData) {
	}

	/**
	 * Uses Javers library to generate diff between two Java objects.
	 * @param left The first object to be compared.
	 * @param right The second object to be compared.
	 * @return The set of differences between the two given objects.
	 */
	@SuppressWarnings("unused")
    public Diff compare(Object left, Object right) {
        return JaversBuilder.javers().build().compare(left, right);
	}

}
