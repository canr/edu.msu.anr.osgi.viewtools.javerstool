package edu.msu.anr.osgi.viewtools.javerstool;

import com.dotcms.repackage.org.osgi.framework.BundleContext;
import com.dotmarketing.osgi.GenericBundleActivator;


/**
 * Activates the OSGi bundle.
 */
public class Activator extends GenericBundleActivator {

    /**
     * Starts the OSGi bundle.
     * @param bundleContext OSGi bundle context.
     * @throws Exception If the bundle cannot register the viewtool.
     */
    @Override
    public void start ( BundleContext bundleContext ) throws Exception {

        //Initializing services...
        initializeServices( bundleContext );

        //Registering the ViewTool service
        registerViewToolService( bundleContext, new JaversToolInfo() );
    }

    /**
     * Stops the OSGi bundle
     * @param bundleContext OSGi bundle context.
     * @throws Exception If the bundle cannot unregister the viewtool.
     */
    @Override
    public void stop ( BundleContext bundleContext ) throws Exception {
        unregisterViewToolServices();
    }

}