package edu.msu.anr.osgi.viewtools.javerstool;

import org.apache.velocity.tools.view.context.ViewContext;
import org.apache.velocity.tools.view.servlet.ServletToolInfo;

/**
 * Provides information about the {@link JaversTool}.
 */
class JaversToolInfo extends ServletToolInfo {

    /**
     * Defines the viewtool's key.
     * @return The viewtool's key.
     */
    @Override
    public String getKey () {
        return "javers";
    }

    /**
     * Defines the viewtool's scope.
     * @return The viewtool's scope.
     */
    @Override
    public String getScope () {
        return ViewContext.APPLICATION;
    }

    /**
     * Defines the name of the class to be loaded.
     * @return The viewtool's class name.
     */
    @Override
    public String getClassname () {
        return JaversTool.class.getName();
    }


    /**
     * Creates and initializes an instance of the viewtool.
     * @param initData The data necessary to initialize the viewtool.
     * @return An initialized instance of the viewtool.
     */
    @Override
    public Object getInstance ( Object initData ) {
        JaversTool viewTool = new JaversTool();
        viewTool.init( initData );

        setScope( ViewContext.APPLICATION );

        return viewTool;
    }

}