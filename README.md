This plugin provides a viewtool for the [Javers](http://javers.org/) library.

# Installation
This plugin must be built from source and deployed to dotCMS' OSGi framework.

## Building
In order to build this plugin, use the included Gradle wrapper to run the 'jar' task.

On Windows:
```
.\gradlew.bat jar
```

On Linux or MacOS:
```
./gradlew jar
```

The resulting .jar file should be located in './build/libs'.

## Deploying
In order to deploy the compiled plugin to dotCMS' OSGi framework, you may run the Gradle "load" task, upload the plugin .jar through dotCMS' Dynamic Plugins portlet, or manually place the plugin in the dotCMS instance's Apache Felix load directory.

### Dynamic Plugins Portlet
Use of dotCMS' Dynamic Plugins portlet is recommended to those without filesystem access to the dotCMS instance.

See [dotCMS' documentation on the Dynamic Plugins portlet](https://dotcms.com/docs/latest/osgi-plugins#Portlet).

### Load Task
This project's build.gradle file defines a task called "load" which builds this plugin, undeploys previous versions, and deploys the new version to the Apache Felix load directory. In order to run this task, you must set the environment variable "FELIXDIR" to the dotCMS instance's Apache Felix base directory. The load task is recommended for those with filesystem access to the dotCMS instance.

```
> FELIXDIR="$dotcms/dotcms_3.3.2/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix"
> export FELIXDIR
> ./gradlew load
```

### Filesystem
Apache Felix monitors its "load" and "undeploy" directories to install and uninstall plugin .jar files. You can move .jar files on the filesystem to load and unload them from the OSGi framework.
```
> mv "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/load/edu.msu.anr.osgi.structuralintegrity.service-*.jar" "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/undeploy"
> cp ./build/libs/*.jar "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/load/"
```

# Usage
The JaversTool can be accessed in a Velocity template using the key
'$javers'.

```vtl
## Create two maps with different values for the same key.
#set($map1 = {"key1": "val1"})
#set($map2 = {"key1": "val2"})

## Use Javers to create a diff between the maps.
#set($diff = $javers.compare($map1, $map2))
<p>$diff</p>
```

Running the above snippet outputs a texual representation of the differences between the two maps.

```
Diff: 1. MapChange{globalId:'org.javers.core.graph.LiveGraphFactory$MapWrapper/', property:'map', entryChanges:[EntryValueChange{key1:'val1'>>'val2'}]}
```
